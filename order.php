<?php
 if ($_POST["page"] == "confirm")
 {
   confirmPage ();
 }
 elseif ($_POST["page"] == "order")
 {
   orderPage();
 }
 else
 {
    orderForm();
 }

 // Display the order form
 function orderForm()
 {
  $script = $_SERVER['PHP_SELF'];
  print <<<PAGE1
  <html>
  <head>
  <title> Pizza Order Form </title>
  </head>
  <body>
  <h3> Pizza Order Form </h3>
  <form method = "post" action = "$script">
  <p>
  <label> Name </label>
  <input type = "text" name = "client"><br>


  <input type = "radio" name = "size" value = "large" />   
  <label> Large </label><br>

  <input type = "radio" name = "size" value = "medium" />   
  <label> Medium </label><br>

  <input type = "radio" name = "size" value = "small" />   
  <label> Small </label><br>
  <input type = "hidden" name = "page" value = "confirm" />
  </p>
  <p>
  <input type = "submit" value = "Place Order" />
  </p>
  </form>
  </body>
  </html>
PAGE1;
  }

  function confirmPage()
  {
    $size = $_POST["size"];
    $name = $_POST["client"];
    $script = $_SERVER['PHP_SELF'];


    $str = $name.":".$size.":";

    print <<<PAGE2
    <html>
    <head>
    <title> Confirm Order </title>
    </head>
    <body>
    <p>
    
    $name ordered a $size pizza. Please confirm. 
    </p>
    <p>
    <form method = "post" action = "$script">
    <input type = "hidden" name = "page" value = "order" />
    <input type = "submit" value = "Confirm Order" />
    <input type = "submit" value = "Cancel" name = "cancel"/>

    <input type = "hidden" name = "client" value = "$name" />
    <input type = "hidden" name = "info" value = "$str" />
    </form>
    </p>
    </body>
    </html>

PAGE2;
  }

  function orderPage()
  {
    $time = date("m-d-y g:i:s a");
    $name = $_POST["client"];
    $str = $_POST["info"] . $time . "\n";
    $script = $_SERVER['PHP_SELF'];

    

    $fh = fopen("./orders.txt", "a");
    fwrite($fh, $str);
    fclose($fh);

    print <<<PAGE3
    <html>
    <head>
    <title> Place Order </title>
    </head>
    <body>
    <p>
    Thank you $name. Your order has been placed.
    </p>
    </body>
    </html>
PAGE3;
  }
?>
