<html>
<head>
<title> Number of Visits </title>
</head>
<body>

<?php
// Determine if the user has visited this site before
if (!isset($_COOKIE["numVisits"])) {
 $numVisits    = 1;
 $lastVisit    = "never";
 $currentVisit = date("m-d g:i:s a");
 setcookie("numVisits", $numVisits, time() + 3600 * 24 * 365);
 setcookie("lastVisit", $currentVisit, time() + 3600 * 24 * 365);
} 
// If the user has visited, add to the number of visits and record the current visit for next time
else {
 $numVisits    = $_COOKIE["numVisits"];
 $lastVisit    = $_COOKIE["lastVisit"];
 $currentVisit = date("m-d g:i:s a");
 $numVisits++;
 setcookie("numVisits", $numVisits, time() + 3600 * 24 * 365);
 setcookie("lastVisit", $currentVisit, time() + 3600 * 24 * 365);
}

// ?>

<h3> My Website </h3>
<?php
print "This is your visit number: " . $numVisits . "<br>"; 
print "The last time you visited this was: " . $lastVisit . "<br>";
print "The current time is: " . $currentVisit . "<br>";
?>

</body>
</html>
